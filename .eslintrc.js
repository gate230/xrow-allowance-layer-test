// eslint-disable-next-line no-undef
module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  rules: {
    'no-multi-spaces': 'off',
    'eqeqeq': 'error',
    'dot-notation': 'error',
    'no-console': 'off',
    'no-loss-of-precision': 'error',
    'space-before-function-paren': ['error', 'always'],
    'curly': ['error', 'multi-or-nest'],
    'comma-dangle': ['error', 'never'],
    'comma-spacing': ['error', { 'before': false, 'after': true }],
    'eol-last': ['error', 'always'],
    'no-multiple-empty-lines': ['error'],
    'no-new-symbol': 'error',
    'no-trailing-spaces': ['error'],
    'no-undef': ['warn'],
    'object-curly-spacing': ['error', 'always'],
    'object-shorthand': 'error',
    'prefer-const': 2,
    'quotes': ['error', 'single'],
    'semi': ['error', 'never'],
    'space-in-parens': ['error', 'never'],
    'strict': [2, 'never']
  },
  env: {
    node: true,
    mocha: true
  },
  parserOptions: {
    ecmaVersion: 'latest'
  }
}
