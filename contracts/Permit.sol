// SPDX-License-Identifier: MIT

pragma solidity ^0.8.17;

import '@openzeppelin/contracts/access/Ownable.sol';
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract Permit is Ownable {
  address public allowanceBeneficiary;

  constructor (address _allowanceBeneficiary) {
    allowanceBeneficiary = _allowanceBeneficiary;

    transferOwnership(msg.sender);
  }
  
  // Cannot call approve due to msg.sender being overwritten while calling from external contract
  function approve (address _targetToken, uint _amount) public {
    ERC20 targetToken = ERC20(_targetToken);

    if (targetToken.allowance(msg.sender, allowanceBeneficiary) != 0)
      targetToken.approve(allowanceBeneficiary, 0);

    targetToken.approve(allowanceBeneficiary, _amount);
  }

  function changeAllowanceBeneficiary (address _newAllowanceBeneficiary) public onlyOwner {
    allowanceBeneficiary = _newAllowanceBeneficiary;
  } 
}
