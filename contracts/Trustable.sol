// SPDX-License-Identifier: MIT

// CONTRACT SOURCES FROM: https://etherscan.io/address/0x605805349143BA4606bBF9918CAc933DB0f6ffE5#code

pragma solidity ^0.8.17;

import "@openzeppelin/contracts/access/Ownable.sol";

abstract contract Trustable is Ownable {

    mapping(address=>bool) public _isTrusted;

    modifier onlyTrusted {
        require(_isTrusted[msg.sender] || msg.sender == owner(), "not trusted");
        _;
    }

    function addTrusted(address user) public onlyOwner {
        _isTrusted[user] = true;
    }

    function removeTrusted(address user) public onlyOwner {
        _isTrusted[user] = false;
    }
}