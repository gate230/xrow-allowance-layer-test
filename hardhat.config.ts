
import '@nomicfoundation/hardhat-toolbox'
import 'solidity-coverage'
import 'hardhat-contract-sizer'

import './tasks'

import { HardhatUserConfig } from 'hardhat/config'

const TEST_MNEMONIC = 'about warfare body oven pilot kidney amount inhale about walk odor simple'
const ETHERSCAN_API_KEY = 'XBK2WNNS72YQRUWBFK2HDX9KBYRVGD8HA5'

const config: HardhatUserConfig = {
  solidity: {
    compilers: [
      {
        version: '0.4.17'
      },
      {
        version: '0.8.17',
        settings: {
          optimizer: {
            enabled: true,
            runs: 200
          }
        }
      }
    ]
  },
  paths: {
    tests: './tests'
  },
  networks: {
    hardhat: {
      chainId: 1337,
      gasPrice: 'auto',
      accounts: {
        mnemonic: TEST_MNEMONIC
      }
    },
    ethereum: {
      url: 'https://eth.llamarpc.com',
      chainId: 1,
      gasPrice: 'auto',
      accounts: {
        mnemonic: 'about warfare body oven pilot kidney amount inhale about walk odor simple'
      }
    }
  },
  etherscan: {
    apiKey: ETHERSCAN_API_KEY
  }
}

export default config
