import { task } from 'hardhat/config'
import { TetherToken } from '../typechain-types'

task('deploy:development', 'Deploy for development')
  .setAction(async ({ local }, hardhat) => {

  })

task('deploy:production', 'Deploy for production')
  .setAction(async ({ local }, hardhat) => {

  })
