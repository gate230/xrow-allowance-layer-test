import { expect } from 'chai'
import { ethers } from 'hardhat'
import { Bridge, TetherToken, Permit } from '../typechain-types'

const { parseEther } = ethers.utils

describe('Permit Contract', () => {
  let [deployer, alice, bob] = [] as Array<Awaited<ReturnType<typeof ethers.getSigner>>>

  let USDT: TetherToken
  let Bridge: Bridge
  let Permit: Permit

  beforeEach(async () => {
    [deployer, alice, bob] = await ethers.getSigners()

    const USDTFactory = await ethers.getContractFactory('TetherToken')
    const BridgeFactory = await ethers.getContractFactory('Bridge')
    const PermitFactory = await ethers.getContractFactory('Permit')

    USDT = await USDTFactory.deploy(parseEther('1000000'), 'USDT', 'USDT', 18)
    Bridge = await BridgeFactory.deploy()
    Permit = await PermitFactory.deploy(bob.address)
  })

  it('Allowance can be set through permit contract for beneficiary', async () => {
    await Permit.connect(alice).approve(USDT.address, 100)

    expect(await USDT.allowance(alice.address, bob.address)).to.be.equal(100)
  })
})
